import random

# Definimos la función principal
def jugar_ahorcado():
    
    # Lista de palabras
    palabras = ["zariguella", "mapache", "elefante", "jirafa", "rinoceronte", "hipopotamo", "cocodrilo", "serpiente", "armadillo", "leopardo"]
    
    # Seleccionar una palabra aleatoria
    palabra = random.choice(palabras)
    
    # Inicializar la variable de vidas restantes
    vidas = 10
    
    # Inicializar la lista de letras correctas
    letras_correctas = []
    
    # Ciclo principal
    while vidas > 0:
        
        # Mostrar la palabra con guiones en lugar de letras no adivinadas
        palabra_mostrada = ""
        for letra in palabra:
            if letra in letras_correctas:
                palabra_mostrada += letra
            else:
                palabra_mostrada += "_"
        print(palabra_mostrada)
        
        # Pedir al usuario que ingrese una letra:
        print("la temática de palabras a adivinar es: animales")
        letra = input("Ingresa una letra: ")
        
        # Verificar si la letra está en la palabra
        if letra in palabra:
            letras_correctas.append(letra)
            print("¡Correcto!")
        else:
            vidas -= 1
            print("Incorrecto. Te quedan {} vidas.".format(vidas))
        
        # Verificar si el usuario ha adivinado la palabra completa
        if set(palabra) == set(letras_correctas):
            print("¡Felicidades! ¡Adivinaste la palabra!")
            return
        
    # Si se acabaron las vidas, mostrar el mensaje de derrota
    print("Lo siento, has perdido. La palabra era {}.".format(palabra))

# Llamar a la función principal
jugar_ahorcado()