import random

def adivina():
    numero= int (random.randint(1,100))
    intentos= 0
    
    while True:
        print("¿Es tu número el",numero, "?")
        respuesta = input("Ingresa si, mayor o menor: ")
        intentos += 1
        
        if respuesta == "si":
            print("¡Genial! Adiviné tu número en", intentos, "intentos.")
            break
        elif respuesta == "mayor":
            numero = numero +1
        elif respuesta == "menor":
            numero = numero -1
        else:
            print("No ingresaste una respuesta válida. Por favor, intenta de nuevo.")

print("Piensa en un número del 1 al 100. Yo trataré de adivinarlo.")
input("Cuando estés listo, presiona Enter para continuar...")
adivina()